# airflow-tutorial

Airflow tutorial

## Getting started
# Export Airflow directory var
export AIRFLOW_HOME=~/antonio

# Create virtualenv
python3 -m venv .venv

# Install requirements
source .venv/bin/activate
python3 -m airflow db init
## Add your files
